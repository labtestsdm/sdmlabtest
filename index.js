//import connection
const connection = require('./connection')

//import express framework
const express = require('express')

//import body-parser
const bodyParser = require('body-parser');
const { serverHandshake } = require('./connection');

//stored functions in a variable 'app'
var app = express();

//use middleware in app.use ---->bodyparser
//data ->in form of json formate
app.use(bodyParser.json())

  

//to fecth details of one movie  using name
app.get('/movie/:movie_title',(req,res)=>{
    connection.query('select * from movieTb where movie_title=?',[req.params.movie_title],(err,rows)=>{
        if(err){
            console.log(err)
        }
        else{
           
            console.log(rows)
           
            res.send(rows)
        }
    })
})


//Delete row from database
app.delete('/movie/:movie_id',(req,res)=>{
    connection.query('delete from movieTb where movie_id=?',[req.params.movie_id],(err,rows)=>{
        if(err){
            console.log(err)
        }
        else{
            console.log(rows)
            res.send(rows)
        }
    })
})


app.post('/movie',(req,res)=>{
    var film = req.body
    var filmData = [film.movie_id, film.movie_title,film.movie_release_date,film.movie_time,film.director_name]
    connection.query('INSERT into movietb(movie_id, movie_title, movie_release_date,movie_time,director_name) values(?)',[filmData],(err,rows)=>{
        if(err){
            console.log(err)
        }
        else{
            console.log(rows)
            res.send(rows)
        }
    })
})



//set port on which express server will start listening
app.listen(3000,()=>console.log("Express server is running in posrt 3000"))